Rails.application.routes.draw do

  resources :nadgodzinies
  post 'zmianas/index', to: 'zmianas#index'
  post 'zmianas/show'
  get 'zmianas/new'
  get 'zmianas/create'
  get 'zmianas/show'
  get 'zmianas/index'
  delete '/zmianas/destroy', to: 'zmianas#destroy'
  get '/zmianas/delete/', to: 'zmianas#destroy'

      resources :users do
          member do
              get :following, :followers
          end
      end
    resources :relationships,       only: [:create, :destroy]
    resources :account_activations, only: [:edit]
    resources :password_resets,     only: [:new, :create, :edit, :update]
    resources :microposts, only: [:create, :destroy]
    
    post '/zmianas/destroy', to: 'zmianas#destroy'
    post '/zmianas', to: 'zmianas#create'
    
    get '/signup', to: 'users#new'
    get '/help', to: 'static_pages#help'
    get '/home', to: 'static_pages#home'
    get '/about', to: 'static_pages#about'
    get '/contact', to: 'static_pages#contact'
    get '/login', to: 'sessions#new'
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#destroy'
    get '/kalendarz', to: 'kalendarz#new'
    get '/kalendarz/new', to: 'kalendarz#new'
    post '/kalendarz/new', to: 'kalendarz#new'
    post '/kalendarz', to: 'kalendarz#create'
    get 'nadgodzinies/new', to: 'nadgodzinies#new'
    get '/nadgodzinies', to: 'nadgodzinies#show'
    get '/users/nadgodzinies/new', to: 'nadgodzinies#new'
    
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
end
#zmianas_path