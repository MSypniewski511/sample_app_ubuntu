class CreateZmianas < ActiveRecord::Migration[5.0]
  def change
    create_table :zmianas do |t|
      t.string :nazwa
      t.string :kalendarz

      t.timestamps
    end
  end
end
