class AddZmianaIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :zmiana, foreign_key: true
  end
end
##############################################################################################