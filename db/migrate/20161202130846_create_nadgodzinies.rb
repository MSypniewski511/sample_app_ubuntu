class CreateNadgodzinies < ActiveRecord::Migration[5.0]
  def change
    create_table :nadgodzinies do |t|
      t.integer :user
      t.date :data

      t.timestamps
    end
  end
end
