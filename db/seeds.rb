# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Zmiana.create!(nazwa: "RT581", kalendarz: 
               "28|prev-month 29|prev-month 30|prev-month 1|current-day 2 3|work-day 4|work-day<tr>
              5 6 7 8 9|work-day 10|work-day 11|work-day<tr>
              12 13 14 15 16|work-day 17|work-day 18|work-day<tr>
              19 20 21 22 23 24|work-day 25<tr>
              26 27 28 29 30 31 1|next-month")
Zmiana.create!(nazwa: "PLA5ABLUE", kalendarz: 
               "28|prev-month 29|prev-month 30|prev-month 1 2 3 4<tr>
              5|night-day 6|night-day 7|night-day 8|night-day 9|night-day 10 11|night-day<tr>
              12|night-day 13|night-day 14|night-day 15|night-day 16 17 18|night-day<tr>
              19|night-day 20|night-day 21|night-day 22 23 24 25<tr>
              26 27|work-day 28|work-day 29 30 31 1|next-month")
Zmiana.create!(nazwa: "PLA5BBLUE", kalendarz: 
               "28|prev-month 29|prev-month 30|prev-month 1 2 3 4|night-day<tr>
              5|night-day 6|night-day 7|night-day 8|night-day 9 10 11<tr>
              12|night-day 13|night-day 14|night-day 15|night-day 16|night-day 17 18|night-day<tr>
              19|night-day 20|night-day 21|night-day 22 23 24 25<tr>
              26 27|work-day 28|work-day 29 30 31 1|next-month")
Zmiana.create!(nazwa: "PLC1-BLUE", kalendarz: 
               "28|prev-month 29|prev-month 30|prev-month 1|prev-month 2|prev-month 3 4<tr>
              5|prev-month 6|prev-month 7|prev-month 8|prev-month 9|prev-month 10 11<tr>
              12|prev-month 13|prev-month 14|prev-month 15|prev-month 16|prev-month 17 18|prev-month<tr>
              19|prev-month 20|prev-month 21|prev-month 22|prev-month 23 24 25<tr>
              26 27|work-day 28|work-day 29 30 31 1|next-month")


User.create!(name: "Maciej Sypniewski", email: "msypniewski511@gmail.com",
             password: "Ms55015501",
             password_confirmation: "Ms55015501",
             admin: true,
             zmiana_id: 1,
             activated: true,
             activated_at: Time.zone.now)
User.create!(name: "Aneta Ligocka", email: "aligocka511@interia.pl",
             password: "chaslo",
             password_confirmation: "chaslo",
             zmiana_id: 2,
             admin: false,
             activated: true,
             activated_at: Time.zone.now)
User.create!(name: "Kamil Ligocki", email: "kligocki511@gmail.com",
             password: "chaslo",
             password_confirmation: "chaslo",
             zmiana_id: 4,
             admin: false,
             activated: true,
             activated_at: Time.zone.now)
User.create!(name:  "Example User",
             email: "example@przyklad.org",
             password: "chaslo",
             password_confirmation: "chaslo",
             zmiana_id: 3,
             admin: false,
             activated: true,
             activated_at: Time.zone.now)
10.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              zmiana_id: 1,
              activated: true,
              activated_at: Time.zone.now)
end
users = User.order(:created_at).take(6)
10.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Following relationships
users = User.all
user  = users.first
following = users[2..20]
followers = users[3..10]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
