json.array!(@nadgodzinies) do |nadgodziny|
  json.extract! nadgodziny, :id, :user, :data
  json.url nadgodziny_url(nadgodziny, format: :json)
end
