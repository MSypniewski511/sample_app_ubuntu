class UsersController < ApplicationController
    
    before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
    before_action :correct_user, only: [:edit, :update]
    before_action :admin_user,     only: :destroy
    
    def index
        @users = User.paginate(page: params[:page], per_page: 10)
    end
    def destroy
        User.find(params[:id]).destroy
        flash[:success] = "Użytkownik został usunięty!"
        redirect_to users_url
    end
    
  def new
      @user = User.new
  end
  def show
      @user = User.find(params[:id])
      @string = Zmiana.find(@user.zmiana_id).kalendarz
      @microposts = @user.microposts.paginate(page: params[:page], per_page: 5)

  end
  
  def create
    @user = User.new(user_params)    # Not the final implementation!
    if @user.save
        @user.send_activation_email
        flash[:info] = "Sprawdż proszę twoją skrzynkę pocztową w celu aktywacji konta."
        redirect_to root_url
        # Handle a successful save.
    else
      render 'new'
    end
  end
  
  def edit
      @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
        flash[:success] = 'Profil zaktualizowany'
        redirect_to @user
      # Handle a successful update.
    else
      render 'edit'
    end
  end
  
  # Confirms a logged-in user.
  #def logged_in_user
   #   unless logged_in?
   #       store_location
   #       flash[:danger] = "Proszę zaloguj się"
   #       redirect_to login_url
   #   end
  #end
  
  # Confirm the corect user.
  def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
  end
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :zmiana_id)
    end
    # Before filters

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    

end
