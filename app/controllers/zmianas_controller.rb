class ZmianasController < ApplicationController
    before_action :admin_user, only: [:index, :edit, :show, :update, :destroy]

    
    # Wywoływanie formularza nowej zmiany:
    def new
        @zmiana = Zmiana.new
    end

    def index
      @zmianas = Zmiana.paginate(page: params[:page], per_page: 10)
    end
    

    
    # Zapisywanie zmiany
  def create
    @zmiana = Zmiana.new(zmiana_params)    # Not the final implementation!
    if @zmiana.save
        flash[:info] = "Zmiana została dodana do bazy."
        redirect_to zmianas_index_url
        # Handle a successful save.
    else
      render 'new'
    end
  end

  def show
      @zmiany = Zmiana.all
  end

  def destroy
    Zmiana.find(params[:id]).destroy
        flash[:success] = "Użytkownik został usunięty!"
        redirect_to zmianas_index_url

  end
  
  private
  
      def zmiana_params
          params.require(:zmiana).permit(:nazwa, :kalendarz)
      end
      
      def kalendarz_params
          params.require(:kalendarz).permit(:kalendarz[])
      end

    # Before filters

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user
    end
end
