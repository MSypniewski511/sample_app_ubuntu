class NadgodziniesController < ApplicationController
  before_action :set_nadgodziny, only: [:show, :edit, :update, :destroy]
  before_action :admin_user,     only: [:destroy, :show]
  # GET /nadgodzinies
  # GET /nadgodzinies.json
  def index
    @nadgodzinies = Nadgodziny.all
  end

  # GET /nadgodzinies/1
  # GET /nadgodzinies/1.json
  def show
  end

  # GET /nadgodzinies/new
  def new
      @user = current_user
      @nadgodziny = Nadgodziny.new
  end

  # GET /nadgodzinies/1/edit
  def edit
  end

  # POST /nadgodzinies
  # POST /nadgodzinies.json
  def create
    @nadgodziny = Nadgodziny.new(nadgodziny_params)

    respond_to do |format|
       
        if
                 @nadgodziny.save
        format.html { redirect_to root_path, notice: 'Zapisałeś się na nadgodziny.' }
        format.json { render :show, status: :created, location: @nadgodziny }
      else
        format.html { render :new }
        format.json { render json: @nadgodziny.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nadgodzinies/1
  # PATCH/PUT /nadgodzinies/1.json
  def update
    respond_to do |format|
      if @nadgodziny.update(nadgodziny_params)
        format.html { redirect_to @nadgodziny, notice: 'Nadgodziny zostały zapisane.' }
        format.json { render :show, status: :ok, location: @nadgodziny }
      else
        format.html { render :edit }
        format.json { render json: @nadgodziny.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nadgodzinies/1
  # DELETE /nadgodzinies/1.json
  def destroy
    @nadgodziny.destroy
    respond_to do |format|
      format.html { redirect_to nadgodzinies_url, notice: 'Zmieniłeś swoje nadgodziny.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nadgodziny
      @nadgodziny = Nadgodziny.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nadgodziny_params
      params.require(:nadgodziny).permit(:user, :data)
    end
end
