class KalendarzController < ApplicationController
  def new
    @zmiana = Zmiana.find(params[:id])
      @string = "28|prev-month 29|prev-month 30|prev-month 1| 2 3 4<tr>5 6 7 8 9 10 11<tr>12 13 14 15 16 17 18<tr>19 20 21 22 23 24 25<tr>26 27 28 29 30 31 1|next-month"
  end

  def create
    pierdolone_id = params[:kalendarz].last
     zmiana = Zmiana.find(pierdolone_id)
     zmiana.kalendarz = oblicz(params[:kalendarz])
     zmiana.save
  end


  def update
  end
  
  def show
    @kal = @kalendarz
  end
  
  private
  
    def zmiana_params
      params.require(:zmiana).permit(:kalendarz)
    end








# metoda aminiająca tablicę kalendarza na string
  def oblicz(kalendarz)
    @kalendarz = kalendarz
    tmp_string = %w{28|prev-month 29|prev-month 30|prev-month 1 2 3 4 
      <tr> 5 6 7 8 9 10 11 <tr> 12 13 14 15 16 17 18 <tr> 19 20 21 22 23 24 25 <tr> 26 27 28 29 30 31 1|next-month}
    i = 0
    a = 0
    wynik = []
    tmp_string.each do |dzien|
      tab = dzien.split('|')
      if @kalendarz[i] == "1" && tab[0] != "<tr>"
        wynik[a] = tab[0].to_s + "|work-day"
      elsif @kalendarz[i] == "2" && tab[0] != "<tr>"
        wynik[a] = tab[0].to_s + "|night-day"
      elsif @kalendarz[i] == "3" && tab[0] != "<tr>"
        wynik[a] = tab[0].to_s + "|another-month"
      else
        wynik[a] = tab[0].to_s if tab[0] != "<tr>"
        wynik[a] = tab[0]
      end
      i += 1 if tab[0] != "<tr>"
      a +=1
    end
    return wynik.join(' ')
  end
end
 

