require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
    def setup
       @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    end
    
    test "Powinien byc valid" do
        assert @user.valid?
    end
    
    test "Zmienna -name- powinna być obecna!" do
        @user.name = ""
        assert_not @user.valid?
    end
    
    test "email should be present" do
        @user.email = ""
        assert_not @user.valid?
    end
    
    test "nazwa nie powinna byc za dluga" do
        @user.name = "a" * 51
        assert_not @user.valid?
    end
    
    test "email adres nie powinien byc za dlugi" do
        @user.email = "a" *255
        assert_not @user.valid?
    end
    
    test "email validation should accept valid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |valid_address|
      @user.email = valid_address
      assert_not @user.valid?, "#{invalid_addresses.inspect} should be valid"
    end
  end
  
  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
end
